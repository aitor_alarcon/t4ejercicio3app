/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T4Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo e inicializo los int
		int X = 40, Y = 20;
		
		// Creo e inicializo los double
		double N = 55.6, M = 25.5;
		
		// Hago todos los c�lculos pedidos
		System.out.println("X = " + X + ", Y = " + Y + ", N = " + N + ", M = " + M);
		
		System.out.println(X + " + " + Y + " = " + (X+Y));
		
		System.out.println(X + " - " + Y + " = " + (X-Y));
		
		System.out.println(X + " * " + Y + " = " + (X*Y));
		
		System.out.println(X + " / " + Y + " = " + (X/Y));
		
		System.out.println(X + " % " + Y + " = " + (X%Y));
		
		System.out.println(N + " + " + M + " = " + (N+M));
		
		System.out.println(N + " - " + M + " = " + (N-M));
		
		System.out.println(N + " * " + M + " = " + (N*M));
		
		System.out.println(N + " / " + M + " = " + (N/M));
		
		System.out.println(N + " % " + M + " = " + (N%M));
		
		System.out.println(X + " + " + N + " = " + (X+M));
		
		System.out.println(Y + " / " + M + " = " + (Y/M));
		
		System.out.println(Y + " % " + M + " = " + (Y%M));
		
		System.out.println("Doble de X = " + (X*2) + "\nDoble de Y = " + (Y*2) + "\nDoble de N = " + (N*2) + "\nDoble de M = " + (M*2));
		
		System.out.println("Suma de todas las variables = " + (X+Y+N+M));
		
		System.out.println("Producto de todas las variables = " + (X*Y*N*M));
	
	}

}
